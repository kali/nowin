build:
	go get ./...
	go build
install:
	sudo cp nowin /usr/local/bin/
install-service:
	sudo cp p0f.service /etc/systemd/system/p0f.service
	sudo cp nowin.service /etc/systemd/system/nowin.service
	sudo systemctl enable p0f
	sudo systemctl enable nowin
run-service:
	sudo systemctl start nowin.service
restart-all:
	sudo systemctl restart p0f && sleep 5 && sudo systemctl restart nowin
