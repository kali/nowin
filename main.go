package main

import (
	"github.com/kalikaneko/gop0f"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
)

const (
	P0F_SOCKET     = "/var/run/p0f.socket"
	WIN_USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36 Edg/95.0.1020.53"
	LISTEN_PORT    = "8000"
)

func main() {
	s := os.Getenv("P0F_SOCKET")
	if s == "" {
		s = P0F_SOCKET
	}
	p0f, err := gop0f.New(s)
	if err != nil {
		panic(err)
	}
	defer p0f.Close()
	handler := getHandlerFromClosure(p0f)
	http.Handle("/auth", handler)
	log.Println("Listening on port " + LISTEN_PORT)
	if err := http.ListenAndServe(":"+LISTEN_PORT, nil); err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func getHandlerFromClosure(p0f *gop0f.GoP0f) http.Handler {

	handleRequest := func(w http.ResponseWriter, r *http.Request) {
		ua := r.Header.Get("User-Agent")
		addr := r.Header.Get("X-Forwarded-For")
		info, _ := p0f.Query(net.ParseIP(addr))
		log.Println(addr + ":" + info.OsName)
		log.Println("user agent: " + ua)
		if strings.ToLower(info.OsName) == "windows" {
			if string(ua) == string(WIN_USER_AGENT) {
				log.Println("Allowed User-Agent")
				w.WriteHeader(http.StatusOK)
			} else {
				w.WriteHeader(http.StatusForbidden)
			}
		} else {
			w.WriteHeader(http.StatusOK)
		}
		return
	}
	return http.HandlerFunc(handleRequest)
}
